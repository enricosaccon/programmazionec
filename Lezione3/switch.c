#include<stdio.h>

int main (){
	int scelta=0;

	printf("Inserisci scelta: ");
	scanf("%d", &scelta);

	switch(scelta) {
		case 1: {
			printf("Somma\n");
			break;
		}
		case 2: {
			printf("Sottrazione\n");
			break;
		}
		case 3: {
			printf("Moltiplicazione\n");
			break;
		}
		case 4: {
			printf("Divisione\n");
			break;
		}
		default: {
			printf("Scelta non corretta\n");
		}
	}
	return 0;
}