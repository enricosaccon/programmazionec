/*
Scrivere un programma che, dati in input due numeri interi, visualizzi, a scelta dell’utente, 
il risultato di una delle quattro operazioni fondamentali (addizione, sottrazione, moltiplicazione, divisione).
*/

#include<stdio.h>

int main (){
	//Dichiaro e inizializzo le variabili, ossia i due numeri in ingresso, la scelta e il risultato
	float num1=0.0;
	float num2=0.0;
	int scelta=0;
	float res=0.0;
	int errore=0; //Questa viene spiegata più in giù nel codice

	//Prendiamo i numeri in ingresso e la scelta
	printf("Primo numero: ");
	scanf("%f", &num1);
	printf("Secondo numero: ");
	scanf("%f", &num2);
	printf("Cosa vuoi fare?\n 1. Addizione\n 2. Sottrazione\n 3. Moltiplicazione\n 4. Divisione\n");
	scanf("%d", &scelta);

	//Usiam uno switch per vedere il valore di scelta:
	switch (scelta){
		case 1: { //Addizione
			res=num1+num2;
			break; //IMPORTANTISSIMO mettere i break all'interno dei case altrimenti lo switch continurebbe anche nei case successivi
		}
		case 2:{ //Sottrazione
			res=num1-num2;
			break;
		}
		case 3:{ //Moltiplicazione
			res=num1*num2;
			break;
		}
		case 4:{ //Divisione
			if (num2==0){ //Controlliamo se la divisione è fattibile o meno
				errore=1;//Tuttavia se entriamo in uno di questi due casi è bene non stampare il risultato successivamente. Possiamo creare una variabile errore in più che ci permette di fare questo.
				if (num1==0){
					printf("Divisione indeterminata\n");
				}
				else {
					printf("Divisione impossibile\n");
				}
			}
			else {
				res=num1/num2;
			}
			break;
		}
		default: {
			printf("Scelta non corretta\n");
		}
	}

	//Non rimane altro che stampare il risultato
	if (!errore){
		printf("Il risultato dell'operazione è: %f\n", res);
	}

	return 0;
}