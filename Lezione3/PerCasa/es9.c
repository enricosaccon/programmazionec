/*
Scrivere un programma che chieda quanti siano i valori che verranno introdotti da tastiera, li chieda tutti e ne stampi la somma e la media.
Questo esercizio non è altro che la generalizzazione di quello di prima. È praticamente già fatto
*/
#include<stdio.h>

int main (){
	//Dichiariamo e inizializziamo la media e val che è il numero di numeri che verranno inseriti
	float media=0.0;
	int val=0;
	float app=0.0; //Creiamo una variabile d'appoggio per prendere il valore. 

	//Prendiam in ingresso il numero di valori
	printf("Quanti sono i valori? ");
	scanf("%d", &val);

	//Siccome sappiamo che sono n elementi, possiamo usare un ciclo for per iterare per n volte:
	int i=0; //Ricordatevi che non tutte le versioni di C permettono la dichiarazione di una variabile all'interno del for e quindi è meglio metterla fuori
	for (i=0; i<val; i++){
		printf("%d ", (i+1)); //Stampiamo il numero a cui siamo arrivati così da avere un'idea; Si noti che i+1 non salva in i il valore i+1, ma lo uso semplicemente perchè sono partito da 0 e voglio che l'utente veda però i numero "normali"
		scanf("%f", &app);
		//Sommo a media il nuovo valore
		media+=app;
	}
	media/=val; //Divido per 100

	printf("Il valore medio è: %f\n", media);
}