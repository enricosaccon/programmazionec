/*
Si scriva un programma che calcoli il fattoriale di un numero intero N dato da tastiera. 
Si ricordi che il fattoriale di un numero n (simbolo n!) viene calcolato con la seguente formula: n!= n * (n-1)*(n-2)* ... * 2 * 1
*/

#include<stdio.h>

int main(){
	//Dichiaro e inizializzo le variabili. Il numero non può essere altro che intero e positivo, e mi aspetto un risultato molto grande, quindi useremo la dimensione massima
	int n=0;
	unsigned long long int res=0; //Non basterà comunque come dimensione

	//Abbiamo detto che vogliamo solo un numero positivo quindi controlliamo che questo sia il caso
	do {
		printf("Inserisci un valore positivo: ");
		scanf("%d", &n);
	}	while (n<0);

	if (n==0){ //Esiste un caso particolare di fattoriale che è 0: 0!=1
		res=1;
	}
	else {
		//A questo punto possiamo usare un ciclo qualsiasi per fare calcolare il risultato. Visto che sappiamo il numero di cicli, conviene il for
		res=n; //Assegno subito n a res così ho un ciclo in meno da fare
		int i=n-1; 
		for (i=n-1; i>0; i--){
			res*=i;
		}
	}

	//Non rimane altro da fare che stampare il risultato
	printf("Il fattoriale di %d è: %llu\n", n, res);

	return 0;
}