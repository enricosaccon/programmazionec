/*
Scrivere un programma che calcoli la media di 100 valori introdotti da tastiera.
La vera sfida qui è inserire i 100 valori da tastiera....
*/

#include<stdio.h>
 
#if 1

int main (){
	//Dichiariamo e inizializziamo l'unica variabile che ci serve ossia media
	float media=0.0;
	float app=0.0; //Creiamo una variabile d'appoggio per prendere il valore. 
	//Sappiamo che sono 100 elementi, quindi possiamo semplicemente sommarli prima tutti e poi dividere per 100, usando quindi una sola variabile.
	//Inoltre siccome sappiamo che sono 100 elementi, possiamo usare un ciclo for per iterare per 100 volte:
	int i=0; //Ricordatevi che non tutte le versioni di C permettono la dichiarazione di una variabile all'interno del for e quindi è meglio metterla fuori
	for (i=0; i<100; i++){
		printf("%d ", (i+1)); //Stampiamo il numero a cui siamo arrivati così da avere un'idea; Si noti che i+1 non salva in i il valore i+1, ma lo uso semplicemente perchè sono partito da 0 e voglio che l'utente veda però i numero "normali"
		scanf("%f", &app);
		//Sommo a media il nuovo valore
		media+=app;
	}
	media/=100; //Divido per 100

	printf("Il valore medio è: %f\n", media);
}

/*
Come testare il programma. Ci sono due modi per farlo:
 - Sebbene non sia l'idea migliore, è un programma lineare e quindi anche inserendo 10 numeri al posto che 100 non dovrebbe essere un problema. Bisogna però ricordarsi di cambiare tutti i 100 in 10, magari si può creare una costante apposta
 - Si cerca su internet come inserire i valori in modo automatico. Questo però funziona bene solo se lanciate il programma da un terminale.
Andiamo quindi per la prima opzione. Cambiate in alto prima di int main #if 1 con #if 0 e potete fare il test con 10 elementi ricompilando
*/

#else 

const int val=100; //Sempre meglio dichiarare le costanti fuori da tutto

int main (){
	//Dichiariamo e inizializziamo l'unica variabile che ci serve ossia media
	float media=0.0;
	float app=0.0; //Creiamo una variabile d'appoggio per prendere il valore. 
	//Sappiamo che sono 100 elementi, quindi possiamo semplicemente sommarli prima tutti e poi dividere per 100, usando quindi una sola variabile.
	//Inoltre siccome sappiamo che sono 100 elementi, possiamo usare un ciclo for per iterare per 100 volte:
	int i=0; //Ricordatevi che non tutte le versioni di C permettono la dichiarazione di una variabile all'interno del for e quindi è meglio metterla fuori
	for (i=0; i<val; i++){
		printf("%d ", (i+1)); //Stampiamo il numero a cui siamo arrivati così da avere un'idea; Si noti che i+1 non salva in i il valore i+1, ma lo uso semplicemente perchè sono partito da 0 e voglio che l'utente veda però i numero "normali"
		scanf("%f", &app);
		//Sommo a media il nuovo valore
		media+=app;
	}
	media/=val; //Divido per 100

	printf("Il valore medio è: %f\n", media);
}

#endif