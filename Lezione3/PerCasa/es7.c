/*
Un’agenzia immobiliare, per incrementare le sue vendite, 
decide di abbassare i prezzi degli appartamenti e affigge la seguente tabella: 
Descrizione zona Prezzo al mq. 
Zona 1 euro 1.500 
Zona 2 euro 1.200 
Zona 3 euro 1.400 
Zona 4 euro 1.300 
Zona 5 euro 1.000 
Scrivere un programma che, date in input le dimensioni dell’appartamento in mq. e 
la zona di appartenenza determini il prezzo dell’appartamento e visualizzarlo a video.
*/

#include<stdio.h>

int main (){
	//Dichiariamo e inizializziamo le variabili.
	float mq=0.0;
	int zona=0;
	float prezzo=0.0;
	int errore=0;

	//Prendiamo i valori. Supponiamo che questa volta sia di vitale importanza che i valori della zona siano corretti e che i mq siano maggiori di 0. Inseriamo quindi tutto in un do-while
	do {
		printf("Inserisci la zona: ");
		scanf("%d", &zona);
		printf("Inserisci i mq: ");
		scanf("%f", &mq);
	} while(mq<0 || //Controlliamo i mq
					(zona!=1 && zona!=2 && zona!=3 && zona!=4 && zona!=5)); //e le zone. Basta che uno dei due (per questo l'OR) sia sbagliato e vogliamo che si ripeta il ciclo

	//facciamo quindi uno switch per le zone. In questo caso non servirebbe un default perchè abbiamo appena controllato che zona deve essere 1,2,3,4 o 5. È però sempre buona cosa metterlo
	switch(zona){
		case 1: {
			prezzo=mq*1500;
			break; //IMPORTANTISSIMO mettere i break all'interno dei case altrimenti lo switch continurebbe anche nei case successivi
		}
		case 2: {
			prezzo=mq*1200;
			break;
		}
		case 3: {
			prezzo=mq*1400;
			break;
		}
		case 4: {
			prezzo=mq*1300;
			break;
		}
		case 5: {
			prezzo=mq*1000;
			break;
		}
		default:{
			printf("Scelta non corretta.\n");
			errore=1; //Sebbene le possibilità che questo possa accadere sono praticamente 0, vale sempre il detto better safe than sorry
		}
	}

	//Non ci rimane altro da fare che stampare il prezzo finale.
	if (!errore){
		printf("Il costo finale è di: %f\n", prezzo);
	}
	return 0;
}