/*
Una nuova compagnia telefonica ha promosso l’offerta “oltre80”.
Alla cifra fissa di 0,10 euro (costo alla risposta) occorre aggiungere la cifra di 
0,15 euro per ogni secondo del tempo della telefonata; però oltre gli 80 secondi 
la tariffa per ogni secondo è di 0,09. Fornito da tastiera il numero dei secondi 
della telefonata, visualizzare il costo totale della chiamata.
*/
/*
EFFETTIVAMENTE il problema poteva essere soggetto di interpretazione:
 - Dopo 80 secondi tutti i secondi costano 0,09e
 - Dopo 80 secondi ogni secondo costa 0,09e
Sebbene negli esercizi per casa le abbia accettate entrambe, nessuna compagnia 
telefonica farebbe la prima interpretazione.
*/
#include<stdio.h>

int main (){
	//Dichiaro e inizializzo le variabili.
	float costo=0.0; 
	int secondi=0; //Il numero di secondi non può che essere intero, quindi meglio non mettere un float

	//Prendiamo il numero di secondi. Visto che i secondi non possono essere negativi (ma possono essere 0), introduciamo un ciclo do-while per essere sicuri di questo
	do {
		printf("Quanti secondi sono passati? ");
		scanf("%d", &secondi);
	} while (secondi<0);

	//Controlliamo i secondi. Notiamo inoltre che la base fissa di 0.10 viene messa solo se la persona risponde, quindi se il numero di secondi fosse 0, non bisognerebbe adebitare nessun costo
	if (secondi!=0){
		costo=0.10;
		if (secondi<=80){
			costo+=secondi*0.15; //Non ci sono problemi, ogni secondo costa 0,15e e possiamo quindi moltiplicare e sommare
		}
		else {
			int sec=secondi-80; //Dichiariamo e inizializziamo una seconda variabile come i secondi oltre gli 80. SEBBENE sia buona cosa dichiarare tutte le variabili all'inizio, in questo caso ci sono delle situazioni in cui non si entra nell'else e pertanto è inutile sprecare memoria.
			//Il prezzo finale è quindi dato dalla base dei 0,10e, dagli 80 secondi per 0,15e e dai restanti secondi per 0,09e
			costo+=(80*0.15+sec*0.09);
			//Nel caso si volesse evitare di usare proprio una variabile in più si poteva scrivere:
			// costo+=(80*0.15+(secondi-80)*0.09);
		}
	}
	//Non è necessario mettere alcun else in quanto costo è già inizializzato a 0

	//Non ci rimane altro da fare che stampare il costo finale:
	printf("Costo totale: %f euro\n", costo);
	return 0;
}
