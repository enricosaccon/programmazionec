/*
Scrivere un programma che richieda N numeri da tastiera e ne calcoli il valore massimo.
*/

#include<stdio.h>

int main (){
	//Dichiaro e inizializzo le variabili
	int n=0; //Il numero di elementi da prendere
	float max=0.0;
	float app=0.0; //Serve per prendere in ingresso un valore

	//Prendiamo i valori:
	printf("Quanti numeri sono? ");
	scanf("%d", &n);

	//Visto che sappiamo il numero di elementi possiamo fare un for:
	int i=0; //Ricordatevi che non tutte le versioni di C permettono la dichiarazione di una variabile all'interno del for e quindi è meglio metterla fuori
	for (i=0; i<n; i++){
		printf("Inserisci un valore: ");
		scanf("%f", &app);
		if (i==0){ //Al primo giro pongo max=app perchè sicuramente quello è il valore massimo
			max=app;
		}
		else {
			if (app>max){ //In tutti gli altri casi controllo prima se app è maggiore di max.
				max=app;
			}
		}
	}

	//Non rimane che stampare il valore massimo
	printf("Il numero maggiore tra quelli inseriti è: %f\n", max);
	return 0;
}
