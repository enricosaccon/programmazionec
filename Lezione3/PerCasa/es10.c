/*
Scrivere un programma che calcoli la media di tutti i valori introdotti da tastiera finchè non ne viene introdotto 
uno non compreso tra 18 e 30, ad esempio 9999 (provare proprio questo valore!). La visualizzazione della media 
deve avvenire solo alla fine (ossia non ogni volta che un valore viene introdotto).
*/

#include<stdio.h>

int main(){
	//Dichiariamo e inizializziamo le variabili
	float media=0.0;
	int n=0; //Questa variabile ci servirà per sapere quanti sono i valori che abbiamo sommato
	float app=0.0; //Ci servirà per prendere il valore in ingresso
	int errore=0;

	//Siccome un giro almeno lo vogliamo fare, useremo un do-while.
	//C'è però un problema! Se dobbiamo sommare ogni volta, ma non vogliamo sommare il numero che rompe il ciclo, ci conviene creare un if al suo interno per fare il controllo su app
	//Se il ciclo si rompe poniamo errore a 1, non aggiungiamo il valore e non incrementiamo n
	do {
		printf("Inserisci un valore: ");
		scanf("%f", &app);
		if (app>18 && app <30){
			media+=app; //Aggiungiamo il valore a media
			n++; //Dobbiamo ricordarci di incrementare di 1 per ogni ciclo.
		}
		else {
			errore=1; //Poniamo errore a vero e usciremo dal ciclo in modo pulito
		}
	} while(!errore);

	//A questo punto possiamo calcolare la media e stamparla. Dobbiamo però stare attenti al caso se n==0
	if (n!=0){
		media/=n;
	}
	//Non serve l'else in quanto media è già a 0 per inizializzione
	printf("La media è: %f\n", media);

	return 0;
}