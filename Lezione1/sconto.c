#include <stdio.h>

int main() {
  //Dichiaro e inizializzo le variabili per i due prezzi e la percentuale
  //E' sempre bene inizializzare le variabili
  float p1=0.0;
  float p2=0.0;
  float percent=0.0;
  //Dichiaro e inizializzo i valori di ritorno
  float nuovo_p1=0.0;
  float nuovo_p2=0.0;
  //Prendo in input i due prezzi e la percentuale
  printf("Dammi il primo prezzo: ");
  scanf("%f", &p1);
  printf("Dammi il secondo prezzo: ");
  scanf("%f", &p2);
  printf("Dammi la percentuale di sconto: ");
  scanf("%f", &percent);
  //Creo due variabili per i nuovi prezzi.
  nuovo_p1=p1-p1*percent/100;
  nuovo_p2=p2-p2*percent/100;
  //Stampo i nuov valori
  printf("I nuovi prezzi sono: %f, %f\n", nuovo_p1, nuovo_p2);
  return 0;
}