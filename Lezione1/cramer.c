#include <stdio.h>

int main() {
  //Dichiaro e inizializzo le variabili
  float a=0.0;
  float b=0.0;
  float c=0.0;
  float d=0.0;
  float e=0.0;
  float f=0.0;
  //Dichiaro e inizializzo le due variabili di ritorno
  float x=0.0;
  float y=0.0;
  //Per prima cosa e' meglio stampare qualcosa che faccia capire 
  //come dare l'input.
  printf("Questo programma risolve tramite Cramer un sistema lineare "\
         "di 2 equazioni e 2 incognite nel seguente formato: "\
         "\n\tax+by=e\n\tcx+dy=f\n");
  //Prendo in input i valori
  printf("Dammi il valore di a: ");
  scanf("%f", &a);
  printf("Dammi il valore di b: ");
  scanf("%f", &b);
  printf("Dammi il valore di c: ");
  scanf("%f", &c);
  printf("Dammi il valore di d: ");
  scanf("%f", &d);
  printf("Dammi il valore di e: ");
  scanf("%f", &e);
  printf("Dammi il valore di f: ");
  scanf("%f", &f);
  //Assegno i valori a x e y.
  x=(e*d-b*f)/(a*d-b*c);
  y=(a*f-e*c)/(a*d-b*c);
  //Stampo i nuov valori
  printf("Il sistema e' risolvibile per x=%f e y=%f\n", x, y);
  return 0;
}