#include <stdio.h>

int main() {
  //Dichiaro e inizializzo le variabili per i tre numeri
  //E' sempre bene inizializzare le variabili
  int num1=0;
  int num2=0;
  int num3=0;
  //Dichiaro ed inizializzo il valore di ritorno
  float media=0.0;
  //Prendo in input i tre numeri
  printf("Dammi il primo numero: ");
  scanf("%d", &num1);
  printf("Dammi il secondo numero: ");
  scanf("%d", &num2);
  printf("Dammi il terzo numero: ");
  scanf("%d", &num3);
  //Creo una variabile in cui verra' salvato il valore della media.
  media=(num1+num2+num3)/3.0;
  //Stampo la media
  printf("La media di %d, %d, %d e': %f \n", num1, num2, num3, media);
  return 0;
}