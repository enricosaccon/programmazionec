#include <stdio.h>

int main() {
  //Dichiaro e inizializzo le variabili per i due numeri
  //E' sempre bene inizializzare le variabili
  int num1=0;
  int num2=0;
  //Prendo in input i due numeri
  printf("Dammi il primo numero: ");
  scanf("%d", &num1);
  printf("Dammi il secondo numero: ");
  scanf("%d", &num2);
  printf("All'inizio i due valori sono num1=%d, num2=%d\n", num1, num2);
  //Creo una variabile di appoggio in cui verra' momentaneamente 
  //salvato il valore di una delle due variabili
  int app=num2;
  //Assegno a num2 il valore di num1 e quindi uso il contenuto di app 
  //per assegnare a num1 il valore di num2
  num2=num1;
  num1=app;
  //Stampo quindi di nuovo num1 e num2
  printf("Alla fine i due valori sono num1=%d, num2=%d\n", num1, num2);
  return 0;
}