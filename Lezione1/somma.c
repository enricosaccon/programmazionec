#include <stdio.h>

int main() {
  //Dichiaro e inizializzo le variabili per i due numeri
  //E' sempre bene inizializzare le variabili
  float num1=0;
  float num2=0;
  //Dichiaro e inizializzo la variabile di ritorno
  float somma=0.0;
  //Prendo in input i due numeri
  printf("Dammi il primo numero: ");
  scanf("%f", &num1);
  printf("Dammi il secondo numero: ");
  scanf("%f", &num2);
  //Creo una variabile per la somma e le assegno il 
  //valore della somma
  somma=num1+num2;
  //Stampo la somma
  printf("La somma %f+%f=%f\n", num1, num2, somma);
  //Al posto di creare una variabile era valido anche 
  //stampare subito il valore della somma:
  // printf("La somma %f+%f=%f\n", num1, num2, (num1+num2));
  return 0;
}