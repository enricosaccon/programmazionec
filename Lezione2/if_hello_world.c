#include<stdio.h>

int main (){
	if (0){ //  0 sta per false, mentre tutto quello che non è 0 è true.
		printf("Questo non verra' mai stampato\n");
	}
	if (1){
		printf("1. Questo verra' stampato\n");
	}
	if ('a'){
		printf("2. Questo verra' stampato\n");
	}
	if ("pluto"){
		printf("3. Questo verra' stampato\n");
	}

	//If accetta anche una variabile e ne controlla il contenuto.
	int valore=0;
	if (valore){
		printf("Questo non verra' stampato\n");
	}
	valore=1;
	if (valore){
		printf("4. Questo verra' stampato\n");
	}
	valore=0;
	/*if (&valore){ //Commentato perche' altrimenti darebbe errore perchè si sta compilando con l'opzione -Werror
									//Si presti attenzione che il compilatore darebbe come warning proprio 
									//"address of 'valore' will always evaluate to 'true'"
		printf("Questo non verra' stampato\n");
	} */

	//AND logico in C si scrive con &&. Ritorna vero se e solo se entrambe le variabili sono vere.
	int a=1, b=1;
	if (a && b){ //True AND true
		printf("5. Questo verra' stampato\n");
	}
	a=0;
	if (a && b){ //False AND true
		printf("Questo non verra' stampato\n");
	}
	a=1;
	b=0;
	if (a && b){ //True AND false
		printf("Questo non verra' stampato\n");
	}
	a=0;
	b=0;
	if (a && b){ //False AND false
		printf("Questo non verra' stampato\n");
	}

	//OR logico in C si scrive con ||. Ritorna vero se almeno una delle due variabili è vera.
	a=1;
	b=1;
	if (a || b){ //True OR true
		printf("6. Questo verra' stampato\n");
	}
	a=0;
	if (a || b){ //False OR true
		printf("7. Questo verra' stampato\n");
	}
	a=1;
	b=0;
	if (a || b){ //True OR false
		printf("8. Questo verra' stampato\n");
	}
	a=0;
	b=0;
	if (a || b){ //False OR false
		printf("Questo non verra' stampato\n");
	}

	//NOT logico in C si scrive con !. Ritorna vero se la variabile in ingresso è falsa, falso se era vero. 
	a=1;
	if (!(a)){ //not(true)=false
		printf("Questo non verra' stampato\n");
	}
	a=0;
	if (!(a)){ //not(false)=true
		printf("9. Questo verra' stampato\n");
	}	

	//Una proposizione logica può essere anche quella che viene usata per verificare una relazione di uguaglianza/disuguaglianza fra due numeri
	int n=0;
	printf("Dammi un numero intero: ");
	scanf("%d", &n);
	if (n>5){
		printf("Il numero è maggiore di 5\n");
	}
	else {
		printf("Il numero non è maggiore di 5\n");
	}

	return 0;
}