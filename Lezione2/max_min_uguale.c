#include<stdio.h>

int main (){
	//Dichiaro e inizializzo le due variabili
	int num1=0;
	int num2=0;
	
	//Prendo i valori delle due variabili
	printf("Dammi il primo numero: ");
	scanf("%d", &num1);
	printf("Dammi il secondo numero: ");
	scanf("%d", &num2);

	//Controllo se num1 è maggiore di num2
	if (num1>num2){//In caso fosse vero dico che num1 è maggiore di num2
		printf("%d è maggiore di %d\n", num1, num2);
	}//Altrimenti controllo se sono uguali
	else if (num1==num2){//Se sono uguali dico che sono uguali 
		printf("I due numeri sono uguali\n");
	}
	else {//Altrimenti dico che num2 è minore di num1.
		printf("%d è minore di %d\n", num1, num2);
	}

	return 0;
}