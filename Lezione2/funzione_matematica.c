/* f(x):
1 se x<4
2 se 4 <= x <= 10
3 se x>10
*/

#include<stdio.h>

int main (){
	//Dichiaro e inizializzo il valore.
	float x=0.0;

	//Prendo in ingresso il valore
	printf("Dammi x: ");
	scanf("%f", &x);

	if (x<4){ //Se x<4 allora stampo 1
		printf("1\n");
	}
	else if (x>=4 && x<=10){ //Se x è nell'intervallo [4, 10] allora stampo 2
		printf("2\n");
	}
	else { //Altrimenti stampo 3
		printf("3\n");
	}

	//Quello di prima corrisponde a:
	if (x<4){ //Se x<4 allora stampo 1
		printf("1\n");
	}
	else { //Altrimenti faccio un nuovo controllo
		if (x>=4 && x<=10){ //Se x è nell'intervallo [4, 10] allora stampo 2
			printf("2\n");
		}
		else { //Altrimenti stampo 3
			printf("3\n");
		}
	}	
	return 0;
}