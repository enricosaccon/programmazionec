#include<stdio.h>
#include<math.h> //Libreria per le funzioni matematiche, ci interessa sqrt() per la radice quadrata.

int main (){
	//Dichiaro e inizializzo le variabili
	float radicando=0.0;
	float risultato=0.0;

	//Prendo in ingresso un valore
	printf("Dammi il radicando: ");
	scanf("%f", &radicando);

	//Controllo se radicando è maggiore o uguale a 0
	if (radicando>=0.0){ //Se va bene faccio la radice e stampo il valore
		risultato=sqrt(radicando);
		printf("risultato: %f\n", risultato);
	}
	else { //Se non va bene allora stampo che è minore di 0 e non va bene
		printf("Il radicando e' minore di 0.\n");
	}

	return 0;
}