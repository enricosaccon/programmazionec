/*
Dopo aver acquisito la quantità e il prezzo unitario di un articolo, 
calcolare prezzo finale applicando uno sconto del 10% se la quantità è superiore a 40.
*/
#include<stdio.h>

int main (){
	//Dichiaro e inizializzo le variabili.
	int qnt=0; //quantità. NO lettere accentate nelle variabili.
	float prezzo=0.0; //Prezzo
	float prezzof=0.0; //prezzo finale

	//Prendo il prezzo la quantità e il prezzo del prodotto.
	printf("Inserisci il prezzo del prodotto: ");
	scanf("%f", &prezzo); //Attenzione ad usare l'operatore & nello scanf in quanto questa funzione vuole l'indirizzo di memoria della variabile e non la variabile
	printf("Inserisci la quantità: ");
	scanf("%d", &qnt);

	//Calcolo subito il prezzo finale, se poi la quantità è maggiore di 40 allora posso togliere un 10%
	prezzof=prezzo*qnt;

	//Controllo se sono stati acquistati più di 40 articoli
	if (qnt>40){
		prezzof-=prezzof/10; //Il 10% di un numero è come dividere per 10. Vogliamo inoltre togliere al prezzo finale una quantità e salvarlo in sè stesso quindi possiamo usare l'operatore -=
	}
	//Non ci serve nessun altro else. Stampiamo il prezzo finale.
	printf("Il prezzo finale è: %f\n", prezzof);

	return 0;
}