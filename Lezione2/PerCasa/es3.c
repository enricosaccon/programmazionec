/*
Dopo aver acquisito 3 numeri interi, trovare il maggiore.
*/
#include<stdio.h>

int main (){
	//Dichiaro e inizializzo le 3 variabili per i numeri
	float num1=0.0;
	float num2=0.0;
	float num3=0.0;
	//In realtà non è necessaria una variabile per max: non è specificato da nessuna parte che bisogna salvare il valore massimo.

	//Prendiamo i 3 valori in ingresso:
	printf("Inserisci il primo numero: ");
	scanf("%f", &num1);
	printf("Inserisci il secondo numero: ");
	scanf("%f", &num2);
	printf("Inserisci il terzo numero: ");
	scanf("%f", &num3);

	//Controllo quindi qual è il valore massimo
	if (num1>num2 && num1>num3){ //Se controllo prima per un valore, allora o è il valore massimo e quindi lo stampo
		printf("Il valore maggiore è: %f\n", num1);
	}
	else{ //Oppure non è il valore massimo e quindi mi basta fare il controllo sugli altri due.
		if (num2>num3){
			printf("Il valore maggiore è: %f\n", num2);
		}
		else {
			printf("Il valore maggiore è: %f\n", num3);
		}
	}

	return 0;
}