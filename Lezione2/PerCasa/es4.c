/*
Inserito un numero da tastiera compreso tra 1 e 7, visualizzare il corrispondente giorno della settimana. Es. 1 = lunedì, 4 = giovedì, ecc...
*/

#include<stdio.h>

int main(){
	//Dichiaro e inizializzo la variabile per il giorno. Essendo che i giorni sono solo numeri 1,2,3,4,5,6,7 la dichiaro intera.
	int giorno=0;

	//Prendiamo in ingresso il valore del giorno. Sebbene mettere un do-while qui sarebbe molto bello, non l'avevamo ancora visto.
	printf("Inserisci il numero del giorno: ");
	scanf("%d", &giorno);

	printf("Il giorno inserito è "); //Questa parte è comune per tutti gli output quindi possiamo risparmiare un po' di caratteri 
	//Non avendo fatto fino a questo momento lo switch, dovremmo fare una serie di if:
	//NON SCRIVETE LE LETTERE ACCENTATE IN OUTPUT
	if (giorno==1){
		printf("lunedi.\n");
	}
	else if (giorno==2){
		printf("martedi.\n");
	}
	else if (giorno==3){
		printf("mercoledi.\n");
	}
	else if (giorno==4){
		printf("giovedi.\n");
	}
	else if (giorno==5){
		printf("venerdi.\n");
	}
	else if (giorno==6){
		printf("sabato.\n");
	}
	else if (giorno==7){
		printf("domenica.\n");
	}
	else {
		printf("sbagliato.\n");
	}

	return 0;
}