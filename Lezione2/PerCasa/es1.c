/*
Calcola lo sconto del 15% sul prezzo di due oggetti. Visualizzare a video: lo sconto e il prezzo scontato.
*/
#include<stdio.h>

int main (){
	//Dichiato e inizializzo le variabili
	int p1=0;
	int p2=0;
	int sconto=0;

	//Prendo i due prezzi
	printf("Inserisci il primo prezzo: ");
	scanf("%d", &p1);
	printf("Inserisci il secondo prezzo: ");
	scanf("%d", &p2);
	printf("Inserisci la percentuale di sconto: ");
	scanf("%d", &sconto);

	//Calcolo lo sconto
	float s1=p1*sconto/100.0;
	float s2=p2*sconto/100.0;
	//Calcolo il prezzo scontato
	float p1s=p1-s1;
	float p2s=p2-s1;

	//Stampo i risultati
	printf("Lo sconto sui due prezzi è rispettivamente: %f, %f. Il prezzo finale è rispettivamente: %f, %f\n", s1, s2, p1s, p2s);
	return 0;
}